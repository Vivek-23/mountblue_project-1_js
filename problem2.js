// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of:
//"Last car is a *car make goes here* *car model goes here*"

const Problem_2 = (inventory) => {
  if (inventory == "" || inventory == [] || inventory == undefined) return [];
  let lastCar = inventory.length - 1;
  if (inventory[lastCar].car_make === undefined || inventory[lastCar].car_model === undefined)
    return [];

  return `Last car is a ${inventory[lastCar].car_make} ${inventory[lastCar].car_model}`;
};
module.exports = Problem_2;
