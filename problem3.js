// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

//Solution

const Problem_3 = (inventory) => {
  if (inventory == "" || inventory == [] || inventory == undefined) return [];
  return inventory.sort((a, b) => {
    let fa = a.car_model.toLowerCase(),
      fb = b.car_model.toLowerCase();
    if (fa < fb) {
      return -1;
    }
    if (fa > fb) return 1;
    return 0;
  });
};

module.exports = Problem_3;
