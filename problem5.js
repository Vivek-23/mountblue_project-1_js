// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

// Solution

const problem4 = require('./problem4');


const Problem_5 = (inventory) => {
  if (inventory == "" || inventory == [] || inventory == undefined) return [];
  let carYear = problem4(inventory);
  let old_car = [];
  for (let i = 0; i < carYear.length; i++) {
    if (carYear[i] < 2000) {
      old_car.push(inventory[i]);
    }
  }
  return old_car;
};

module.exports = Problem_5;
